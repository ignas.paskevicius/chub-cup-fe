import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {UserModule} from './modules/user/user.module';
import {ScoreModule} from './modules/score/score.module';
import {ResultsModule} from './modules/results/results.module';
import {EventModule} from './modules/event/event.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { UserBoardComponent } from './user-board/user-board.component';
import {authInterceptorProviders} from './helpers/auth.interceptor';
import {RouterModule} from '@angular/router';
import { InformationComponent } from './information/information.component';
import {FishingZoneModule} from './modules/fishing-zone/fishing-zone.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UserBoardComponent,
    InformationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    UserModule,
    ScoreModule,
    ResultsModule,
    EventModule,
    FishingZoneModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
