import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {CatchScoreAddRequest} from '../models/CatchScoreAddRequest';
import {Observable} from 'rxjs';
import {CatchScoreImageResponse} from '../models/CatchScoreImageResponse';

@Injectable({
  providedIn: 'root'
})
export class AddCatchService {
  private baseUrlCatch = environment.apiUrl + '/api/v1/catch-score';

  constructor(private http: HttpClient) { }

  addCatch(catchScore: CatchScoreAddRequest, selectedImage: File): Observable<any> {
    const formData = new FormData();
    formData.append('image', selectedImage, selectedImage.name);
    formData.append('length', catchScore.length.toString());
    return this.http.post(this.baseUrlCatch, formData);
  }

  getImage(id: number): Observable<CatchScoreImageResponse> {
    return this.http.get<CatchScoreImageResponse>(`${this.baseUrlCatch}/${id}`);
  }
}
