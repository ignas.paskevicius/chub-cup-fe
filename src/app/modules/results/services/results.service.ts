import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Results} from '../models/Results';
import {ResultsBigFish} from '../models/ResultsBigFish';
import {ParticipantResults} from "../models/ParticipantResults";

@Injectable({
  providedIn: 'root'
})
export class ResultsService {
  private baseUrl = environment.apiUrl + '/api/v1/results';

  constructor(private http: HttpClient) { }

  getResults(): Observable<Results[]> {
    return this.http.get<Results[]>(this.baseUrl);
  }

  getBigFish(): Observable<ResultsBigFish>{
    return this.http.get<ResultsBigFish>(this.baseUrl + '/big-fish');
  }

  getParticipantResults(userId: number): Observable<ParticipantResults>{
    return this.http.get<ParticipantResults>(`${this.baseUrl}/${userId}`);
  }
}
