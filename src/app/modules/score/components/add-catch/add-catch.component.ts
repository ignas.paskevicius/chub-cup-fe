import { Component, OnInit } from '@angular/core';
import {RoleEnum} from '../../../../enums/RoleEnum';
import {AddCatchService} from '../../services/add-catch.service';
import {User} from '../../../user/models/User';
import {CatchScoreAddRequest} from '../../models/CatchScoreAddRequest';
import {RoleService} from '../../../user/services/role.service';
import {FormControl, Validators} from '@angular/forms';
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-catch',
  templateUrl: './add-catch.component.html',
  styleUrls: ['./add-catch.component.css']
})
export class AddCatchComponent implements OnInit {

  role = RoleEnum.ROLE_PARTICIPANT;
  users: User[] = [];
  catchScore: CatchScoreAddRequest = {length: null};
  selectedImage: File;
  reader: FileReader = null;
  rateControl = new FormControl('', [Validators.min(25)]);

  imageSrc = './assets/uploadImage.png';

  constructor(
    private addCatchService: AddCatchService,
    private roleService: RoleService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.roleService.getUsersByRole(this.role).subscribe(data => this.users = data);
  }

  onFileChanged(event): void {
    const reader = new FileReader();
    this.selectedImage = event.target.files[0];
    reader.readAsDataURL(this.selectedImage);
    reader.onload = () => {
      this.imageSrc = reader.result as string;
    };
  }

  addCatch(): void{
    this.addCatchService.addCatch(this.catchScore, this.selectedImage).subscribe(data => this.openResultsPage());
  }

  openResultsPage(): void {
    this.router.navigate(['/results']).then(r => console.log('Results....'));
  }
}
