import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {UserCreateRequest} from '../modules/user/models/UserCreateRequest';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl = environment.apiUrl + '/api/v1/auth';

  constructor(private http: HttpClient) { }

  login(credentials): Observable<any> {
    return this.http.post(this.baseUrl + '/signin', {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }

  register(user: UserCreateRequest): Observable<any> {
    return this.http.post(this.baseUrl + '/signup', user, httpOptions);
  }
}
