import { Component, OnInit } from '@angular/core';
import {EventCreateRequest} from '../../models/EventCreateRequest';
import {EventService} from '../../services/event.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {
  event: EventCreateRequest = {eventTitle: '', eventStartDateTime: null, eventEndDateTime: null};

  constructor(
    private eventService: EventService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  openEventListPage(pageName: string): void {
    this.router.navigate([pageName]).then(r => console.log('Opening event list...'));
  }

  addEvent(): void {
    this.eventService.addEvent(this.event).subscribe(data => this.openEventListPage('/event'));
  }
}
