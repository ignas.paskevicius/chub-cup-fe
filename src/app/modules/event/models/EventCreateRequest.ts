import DateTimeFormat = Intl.DateTimeFormat;

export interface EventCreateRequest{
  eventTitle: string;
  eventStartDateTime: DateTimeFormat;
  eventEndDateTime: DateTimeFormat;
}
