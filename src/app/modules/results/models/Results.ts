import {User} from './User';

export interface Results{
  user: User;
  catchCount: number;
  scoreSum: number;
  bigFish: number;
}
