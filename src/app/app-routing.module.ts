import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateEventComponent} from './modules/event/components/create-event/create-event.component';
import {EventListComponent} from './modules/event/components/event-list/event-list.component';
import {UserListComponent} from './modules/user/components/user-list/user-list.component';
import {RegisterUserComponent} from './modules/user/components/register-user/register-user.component';
import {UserEditComponent} from './modules/user/components/user-edit/user-edit.component';
import {ResultsListComponent} from './modules/results/components/results-list/results-list.component';
import {AddCatchComponent} from './modules/score/components/add-catch/add-catch.component';
import {ParticipantResultsComponent} from './modules/results/components/participant-results/participant-results.component';
import {LoginComponent} from './login/login.component';
import {UserBoardComponent} from './user-board/user-board.component';
import {RegisterComponent} from './register/register.component';
import {HomeComponent} from './home/home.component';
import {InformationComponent} from './information/information.component';
import {MapComponent} from './modules/fishing-zone/components/map/map.component';

const routes: Routes = [
  {path: 'create-event', component: CreateEventComponent},
  {path: 'event', component: EventListComponent},
  {path: 'user', component: UserListComponent},
  {path: 'register-user', component: RegisterUserComponent},
  {path: 'edit-user/:id', component: UserEditComponent},
  {path: 'results', component: ResultsListComponent},
  {path: 'add-catch', component: AddCatchComponent},
  {path: 'participant-results/:id', component: ParticipantResultsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'home', component: HomeComponent},
  {path: 'information', component: InformationComponent},
  {path: 'fishing-zone', component: MapComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
