import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Event} from '../models/Event';
import {EventCreateRequest} from '../models/EventCreateRequest';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  private baseUrl = environment.apiUrl + '/api/v1/event';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Event[]> {
    return this.http.get<Event[]>(this.baseUrl);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  addEvent(eventCreateRequest: EventCreateRequest): Observable<any> {
    return this.http.post(this.baseUrl, eventCreateRequest);
  }
}
