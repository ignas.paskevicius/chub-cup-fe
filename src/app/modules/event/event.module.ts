import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EventListComponent} from './components/event-list/event-list.component';
import {CreateEventComponent} from './components/create-event/create-event.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [EventListComponent, CreateEventComponent],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class EventModule { }
