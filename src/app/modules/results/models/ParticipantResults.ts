import {User} from './User';
import {Catch} from './Catch';

export interface ParticipantResults{
  user: User;
  catchScoreList: Catch[];
}
