import { Component, OnInit } from '@angular/core';
import {TestService} from '../services/test.service';

@Component({
  selector: 'app-user-board',
  templateUrl: './user-board.component.html',
  styleUrls: ['./user-board.component.css']
})
export class UserBoardComponent implements OnInit {

  content: string;

  constructor(private testService: TestService) { }

  ngOnInit(): void {
    this.testService.getUserBoard().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }

}
