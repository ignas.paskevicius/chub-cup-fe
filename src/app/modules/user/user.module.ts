import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserListComponent} from './components/user-list/user-list.component';
import {UserEditComponent} from './components/user-edit/user-edit.component';
import {RegisterUserComponent} from './components/register-user/register-user.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [UserListComponent, UserEditComponent, RegisterUserComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
  ]
})
export class UserModule { }
