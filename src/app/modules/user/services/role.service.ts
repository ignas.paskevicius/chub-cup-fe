import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Role} from '../models/Role';
import {User} from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private baseUrl = environment.apiUrl + '/api/v1/role';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Role[]> {
    return this.http.get<Role[]>(this.baseUrl);
  }

  getUsersByRole(role: string): Observable<User[]>{
    return this.http.get<User[]>(`${this.baseUrl}/${'by?role=' + role}`);
  }
}
