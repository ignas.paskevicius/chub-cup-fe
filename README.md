# Chub Cup FE

# Launching application

- Run ng serve to start an application;

# Running an application

- If DB data script from BE project were executed then you can login with those users:

1. Username: admin, password: admin123, role: Administrator;
2. Username: jonas, password: pass123, role: Participant;
3. Username: petras, password: pass123, role: Participant; 


- Login with administrator to create, delete or edit users;

- To add catch login with participant;