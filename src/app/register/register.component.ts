import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {UserCreateRequest} from '../modules/user/models/UserCreateRequest';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: UserCreateRequest = {username: '', password: '', firstName: '', lastName: ''};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(
    private authService: AuthService) { }

  ngOnInit(): void {}

  onSubmit(): void {
    this.authService.register(this.user).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}
