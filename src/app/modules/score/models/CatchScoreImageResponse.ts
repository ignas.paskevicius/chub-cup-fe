export interface CatchScoreImageResponse{
  imageName: string;
  imageType: string;
  bytes: string;
}
