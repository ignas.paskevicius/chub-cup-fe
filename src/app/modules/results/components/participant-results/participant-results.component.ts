import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ResultsService} from '../../services/results.service';
import {ParticipantResults} from '../../models/ParticipantResults';

@Component({
  selector: 'app-participant-results',
  templateUrl: './participant-results.component.html',
  styleUrls: ['./participant-results.component.css']
})
export class ParticipantResultsComponent implements OnInit {

  private sub: any;
  userId: number;
  participantResults: ParticipantResults = {user: {id: null, firstName: '', lastName: ''}, catchScoreList: []};
  imageType: any;
  base64Data: any;
  retrievedImage: any;

  constructor(
    private route: ActivatedRoute,
    private resultsService: ResultsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.userId = params.id;
    });
    this.getParticipantResults(this.userId);
  }

  getParticipantResults(userId: number): void {
    this.resultsService.getParticipantResults(userId).subscribe(data => {
      this.participantResults = data;
      console.log(this.participantResults);
    });
  }

  getImage(bytes: string, type: string): any{
    this.base64Data = bytes;
    this.imageType = type;
    return this.retrievedImage = 'data:' + this.imageType + ';base64,' + this.base64Data;
  }

  backToResults(): void {
    this.router.navigate(['/results']).then(r => console.log('Opening results....'));
  }
}
