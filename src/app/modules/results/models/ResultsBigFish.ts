import {User} from './User';
import {Catch} from './Catch';

export interface ResultsBigFish{
 catchScore: Catch;
 user: User;
}
