export interface UserCreateRequest{
  username: string;
  password: string;
  firstName: string;
  lastName: string;
}
