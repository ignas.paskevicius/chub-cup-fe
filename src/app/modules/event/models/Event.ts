export interface Event{
  id: number;
  eventTitle: string;
  eventStartDateTime: Date;
  eventEndDateTime: Date;
}
