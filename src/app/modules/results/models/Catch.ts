export interface Catch{
  id: number;
  length: number;
  score: number;
  catchDateTime: Date;
  imageName: string;
  imageType: string;
  bytes: string;
}
