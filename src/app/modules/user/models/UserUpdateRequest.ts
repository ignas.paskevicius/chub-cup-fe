import {Role} from "./Role";

export interface UserUpdateRequest{
  id: number;
  firstName: string;
  lastName: string;
  role: Role;
}
