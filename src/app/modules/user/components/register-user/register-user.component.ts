import { Component, OnInit } from '@angular/core';
import {UserCreateRequest} from '../../models/UserCreateRequest';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  user: UserCreateRequest = {username: '', password: '', firstName: '', lastName: ''};

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  openUserListPage(pageName: string): void {
    this.router.navigate([pageName]).then(r => console.log('Opening event list...'));
  }

  addUser(): void {
    this.userService.addUser(this.user).subscribe(data => this.openUserListPage('/user'));
  }
}
