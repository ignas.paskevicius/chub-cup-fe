import { Component, OnInit } from '@angular/core';
import {Event} from '../../models/Event';
import {EventService} from '../../services/event.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  events: Event[];

  constructor(
    private eventService: EventService,
    private router: Router
  ) { }

  openCreateEventPage(pageName: string): void {
    this.router.navigate([pageName]).then(r => console.log('Hello'));
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.eventService.getAll().subscribe(data => this.events = data);
  }

  delete(id: number): void {
    this.eventService.delete(id).subscribe(data => this.getData());
    console.log('User ID: ' + id);
  }
}
