import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddCatchComponent} from './components/add-catch/add-catch.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [AddCatchComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ScoreModule { }
