import { Component, OnInit } from '@angular/core';
import {Results} from '../../models/Results';
import {ResultsService} from '../../services/results.service';
import {ResultsBigFish} from '../../models/ResultsBigFish';
import {Router} from '@angular/router';

@Component({
  selector: 'app-results-list',
  templateUrl: './results-list.component.html',
  styleUrls: ['./results-list.component.css']
})
export class ResultsListComponent implements OnInit {
  results: Results[];
  bigFish: ResultsBigFish = {
    catchScore: {id: null, length: null, score: null, catchDateTime: null, imageType: '', bytes: '', imageName: ''},
    user: {id: null, firstName: '', lastName: ''}
  };
  imageType: any;
  base64Data: any;
  retrievedImage: any;
  currentTime: Date;

  constructor(
    private resultsService: ResultsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getResults();
    this.getBigFish();
  }

  getResults(): void {
    this.resultsService.getResults().subscribe(data => this.results = data);
  }

  getBigFish(): void {
    this.resultsService.getBigFish().subscribe(data => this.bigFish = data);
    this.getImage(this.bigFish.catchScore.bytes, this.bigFish.catchScore.imageType);
  }

  openParticipantResults(userId: number): void {
    this.router.navigate(['/participant-results', userId]).then(r => console.log('Opening participant results....'));
  }

  getImage(bytes: string, type: string): any{
    this.base64Data = bytes;
    this.imageType = type;
    return this.retrievedImage = 'data:' + this.imageType + ';base64,' + this.base64Data;
  }
}
