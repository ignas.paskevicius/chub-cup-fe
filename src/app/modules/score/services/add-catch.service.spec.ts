import { TestBed } from '@angular/core/testing';

import { AddCatchService } from './add-catch.service';

describe('AddCatchService', () => {
  let service: AddCatchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddCatchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
