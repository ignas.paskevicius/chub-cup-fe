import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ResultsListComponent} from './components/results-list/results-list.component';
import { ParticipantResultsComponent } from './components/participant-results/participant-results.component';



@NgModule({
  declarations: [ResultsListComponent, ParticipantResultsComponent],
  imports: [
    CommonModule
  ]
})
export class ResultsModule { }
